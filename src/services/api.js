import axios from 'axios'
import store from '../store'

let api = axios.create({
  baseURL: 'https://apiurl.tld',
  header: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  }
})
api.interceptors.request.use(
    config => {
      if (config.url === "//cvrapi.dk/api") {
// Send ikke nedenstående headers til cvrapi.dk
        return config
      }
      const user = JSON.parse(localStorage.getItem("user"))
      if (user) {
        config.headers.common["Authorization"] = 'Bearer ' + user.accessToken
      }
      return config
    },
    error => {
      console.log('error here')
      return Promise.reject(error)
    }
)

api.interceptors.response.use(
    response => {
      if (response.status === 200 || response.status === 201) {
        return Promise.resolve(response)
      } else {
        return Promise.reject(response)
      }
    },
    error => {
      let response = error.response.data
      if (error.response.status) {
        switch (true) {
          case response.error === 400:
            console.log('ikke logget ind')
            break;
          case response.error === 401:
            console.log('ikke logget ind')
            break;
          case response.error === 403:
            break;
          case response.error === 498:
            store.dispatch('auth/sessionExpired')
            break;
          case response.error === 404:
            store.state.pageState.status = 404
            break;
          case response.error === 502:
            break;
        }
        return Promise.reject(error.response)
      }
    }
)

export default api