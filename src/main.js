import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import vuetify from '@/plugins/vuetify' // path to vuetify export

Vue.config.productionTip = true

new Vue({
  router,
  store,
  vuetify,
  data(){
    return {
      drawer:true, // Se under nav v-model  og app-bar har en toggle-knap
    }
  },
  render: h => h(App)
}).$mount('#app')
